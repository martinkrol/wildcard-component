import { Component, ComponentFactoryResolver, ViewContainerRef, ViewChild, Input } from '@angular/core';

@Component({
  selector: 'wildcard',
  templateUrl: './wildcard.component.html',
  styleUrls: ['./wildcard.component.css']
})
export class WildcardComponent {

  @ViewChild('childComponent', { read: ViewContainerRef }) childComponent: ViewContainerRef;

  @Input('component') component: Component;

	constructor(private componentFactoryResolver: ComponentFactoryResolver){
  }

  loadComponent(component){
      this.childComponent.clear();
      let componentFactory = this.componentFactoryResolver.resolveComponentFactory(component);  
      this.childComponent.createComponent(componentFactory);
  }

  ngAfterViewInit(){
    if (this.component){
      this.loadComponent(this.component.constructor);
    }
  }

}
