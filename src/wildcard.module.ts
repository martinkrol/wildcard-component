import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { WildcardComponent } from './wildcard.component';

@NgModule({
  declarations: [
    WildcardComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [WildcardComponent],
  exports: [
    WildcardComponent
  ]
})
export class WildcardModule { }
